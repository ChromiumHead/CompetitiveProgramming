package Practice;

public class StringBufferPractice {
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("GeeksGeeks");
        sb.insert(5, "For");
        int firstlen = sb.length();
        char[] chr = { 'I', 's', 'G', 'o', 'o', 'd' };
        sb.insert(sb.length(), chr);
        // To reverse the string:
        int seclen = sb.length();
//        sb.reverse();
        sb.delete(firstlen, seclen);
//        sb.deleteCharAt(firstlen-1);
        sb.replace(5, 8, "Are");
        System.out.println(sb);
    }
}
