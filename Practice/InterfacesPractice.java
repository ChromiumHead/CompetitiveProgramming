// Implementation of a stack with fixed size
package Practice;

interface IntStack{
    void push(int x);
    int pop();
}

class FixedStack implements IntStack{
    private int[] stack;
    private int tos;

    FixedStack(int size){
        stack = new int[size];
        tos = -1;
    }

    public void push(int x){
        if(tos == stack.length-1)
            System.out.println("Stack is full!");
        else{
            stack[++tos] = x;
        }
    }

    public int pop(){
        return stack[tos--];
    }
}

public class InterfacesPractice {
    public static void main(String[] args) {
        FixedStack s1 = new FixedStack(5);
        // pushing numbers into the stack
        for(int i=0; i<5; i++){
            s1.push(i+1);
        }

        //printing the stack
        for (int i=0; i<5; i++){
            System.out.println(s1.pop());
        }
    }
}
