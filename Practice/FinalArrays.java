package Practice;

public class FinalArrays {
    int p = 10;

    public static void main(String[] args) {
        final FinalArrays fa = new FinalArrays();
        fa.p = 20;
        System.out.println(fa.p);
    }
}

/*
Java Program to Illustrate Final Arrays
Where Compiler Error Is Thrown
*/

// Main class
//class GFG {
//
//    int p = 20;
//
//    // Main driver method
//    public static void main(String args[])
//    {
//
//        // Creating objects of above class
//        final GFG t1 = new GFG();
//        GFG t2 = new GFG();
//
//        // Assigning values into other objects
//        t1 = t2;
//
//        System.out.println(t1.p);
//    }
//}
