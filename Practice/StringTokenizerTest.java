package Practice;

import java.util.StringTokenizer;

public class StringTokenizerTest {
    public static void main(String[] args) {
        StringTokenizer st = new StringTokenizer("This is a test for String tokenizer", "a");
        while (st.hasMoreTokens())
            System.out.println(st.nextToken());
    }
}
