package Practice;

class A{
    public interface NestedIF{
        boolean isNotNegative(int x);
    }
}

class B implements A.NestedIF{
    @Override
    public boolean isNotNegative(int x) {
        if(x<0)
            return false;
        else
            return true;
    }
}

public class NestedInterfaces {
    public static void main(String[] args) {
        A.NestedIF anif = new B();
        if(anif.isNotNegative(10))
            System.out.println("Not a negative number");
        else System.out.println("Negative number");
    }
}
