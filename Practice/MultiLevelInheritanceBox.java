package Practice;

class BoxClass {
    private double width;
    private double length;
    private double depth;

    BoxClass(double width, double length, double depth) {
        this.width = width;
        this.length = length;
        this.depth = depth;
    }

    BoxClass() {
        width = -1;
        length = -1;
        depth = -1;
    }

    BoxClass(BoxClass ob){
        width = ob.width;
        length = ob.length;
        depth = ob.depth;
    }

    BoxClass(double len){
        width = length = depth = len;
    }

    double volume(){
        return width*length*depth;
    }
}

class BoxWeight extends BoxClass{
    double weight;

    BoxWeight(BoxWeight ob){
        super(ob);
        weight = ob.weight;
    }

    BoxWeight(double width, double length, double depth, double weight) {
        super(width, length, depth);
        this.weight = weight;
    }

    BoxWeight() {
        super();
        this.weight = -1;
    }

    BoxWeight(double len, double weight) {
        super(len);
        this.weight = weight;
    }
}

class Shipment extends BoxWeight{
    double cost;

    Shipment(Shipment ob) {
        super(ob);
        cost = ob.cost;
    }

    Shipment(double width, double length, double depth, double weight, double cost) {
        super(width, length, depth, weight);
        this.cost = cost;
    }

    Shipment(){
        super();
        cost = -1;
    }

    Shipment(double len, double weight, double cost) {
        super(len, weight);
        this.cost = cost;
    }
}

class MultiLevelInheritance{
    public static void main(String[] args) {
        Shipment shipment1 = new Shipment();
        Shipment shipment2 = new Shipment();
        double vol;
        vol = shipment1.volume();
        System.out.println("Volume of shipment1 is " + vol);
        System.out.println("Weight of shipment1 is " + shipment1.weight);
        System.out.println("Shipping cost: $" + shipment1.cost);
        System.out.println();
        vol = shipment2.volume();
        System.out.println("Volume of shipment2 is " + vol);
        System.out.println("Weight of shipment2 is " + shipment2.weight);
        System.out.println("Shipping cost: $" + shipment2.cost);
    }
}