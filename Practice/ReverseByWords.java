package Practice;

import java.util.Arrays;
import java.util.Scanner;

public class ReverseByWords {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        String[] str = input.split(" ");
        String[] ans = new String[str.length];
        int x = str.length - 1;
        for(int i=0; i< str.length; i++)
            ans[i] = str[x--];
        System.out.println(String.join(" ", ans));
    }
}

// The sky is blue