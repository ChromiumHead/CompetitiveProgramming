import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.abs;
import static java.lang.System.out;
import java.io.*;
import java.util.*;

public class template {
    static class FastReader{
        BufferedReader br;
        StringTokenizer st;

        // For Faster Inputs
        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        // Works like Scanner's next() but faster
        String next(){
            while (st == null || !st.hasMoreElements()){
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        // Works like Scanner's nextLine() but faster
        String nextLine(){
            String str = "";
            try{
                if (st.hasMoreElements())
                    str = st.nextToken("\n");
                else str = br.readLine();
            } catch (IOException e){
                e.printStackTrace();
            }
            return str;
        }

        // Works like Scanner's nextInt() but faster
        int nextInt(){
            return Integer.parseInt(next());
        }

        // Works like Scanner's nextLong() but faster
        long nextLong(){
            return Long.parseLong(next());
        }

        // Works like Scanner's nextDouble() but faster
        double nextDouble(){
            return Double.parseDouble(next());
        }
    }

    static boolean isPrime(int x){
        if(x<=1)
            return false;
        for(int i=2; i<=x/2; i++)
            if(x%i==0)
                return false;
        return true;
    }

    public static void main(String[] args) {
        // Main Code
        FastReader fr = new FastReader();
        int T = fr.nextInt();
        while(T-->0){
//            int a = fr.nextInt();
//            int b = fr.nextInt();
//            int c = fr.nextInt();
//            int d = fr.nextInt();
        }
    }
}

